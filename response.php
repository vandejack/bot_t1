<?php
$k_path_url = NULL;
set_time_limit(100000);
require 'vendor/autoload.php';
require_once 'vendor/PHPExcel/PHPExcel/IOFactory.php';
require_once 'vendor/PHPExcel/PHPExcel.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();


require 'vendor/html2pdf/html2pdf.class.php';
define('TOKEN',getenv('TOKEN_BOT'));
date_default_timezone_set('Asia/Makassar');

use Telegram\Bot\Api;

    $tgram = new Api(TOKEN);
    $offset = null;
	$keyboard = [
    'inline_keyboard' => [
			[
				['text' => 'help', 'callback_data' => '/help'],
			]
		]
	];
	$encodedKeyboard = json_encode($keyboard);
    while (true) {
      echo "[D] Retrieving Updates\n";
	  try {
      $response = $tgram->getUpdates(['offset' => $offset]);
	   
      if (count($response)) {

        $offset = $response[count($response)-1]->getUpdateId() +1;
        foreach($response as $r){
		 if (isset($r['callback_query'])){
				$mm = $r['callback_query'];
				$chatid = $mm['from']['id'];
				$chat = ($mm['data']);
				$token = explode(' ',$chat);
				$command = strtolower($token[0]);
				echo $command."\n";
				$path = 'handler'.$command.'/index.php';
				if (file_exists($path)) include $path;
				echo $chat."\n";

		  } else {
		  
          $m = $r->getMessage();
		 
          if ($m == NULL) continue;
			
			$chat = ($m->getText());
			$chatid = $m->getChat()->getId();
			if (isset($m['chat']['title']))
			$title = $m['chat']['title'];
			$bot_username = $m['chat']['username'];
			echo $bot_username."\n";
			// $log = mysql_query("insert into bot_log (chat,chatid) value ('".$chat."','".$chatid."')");
		 
			$token = explode(' ',$chat);
		  
			$command = strtolower($token[0]);
			echo $command."\n";
			$path = 'handler'.$command.'/index.php';
			if (file_exists($path)) include $path;
          //echo "[D] chat: {$chat} id: {$chatid}\n";
          //echo "[D] type: {$m->getFrom()} group: {$chatid}\n";
		  }
        
        }
      }
	  
      else {
        echo "[D] EMPTY RESPONSE\n";
      }
      echo "[D] SLEEP 5s\n";
	  } catch (Exception $e) {
		  echo "[ERROR] FAILED TO CONNECT\n";
		}
      sleep(5);
		@mysqli_close();
     
     
    }

