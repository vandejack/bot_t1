<?php

  function checkwo_getdata($con,$id){
        $content = "ORDER TRACKER ".$id."\n";


    $query = mysqli_query($con,'
      SELECT
		b.*,
		a.*,
		d.uraian,
        a.orderStatus,
        a.orderId,
        c.catatan,
        a.orderName,
        c.kordinat_pelanggan,
        a.orderDate,
        b.updated_at,
		e.laporan_status as status_teknisi
      FROM
        Data_Pelanggan_Starclick a
      LEFT JOIN dispatch_teknisi b ON a.orderId = b.Ndem
      LEFT JOIN psb_laporan c ON b.id = c.id_tbl_mj
	  LEFT JOIN regu d ON d.id_regu = b.id_regu
      LEFT JOIN psb_laporan_status e ON c.status_laporan = e.laporan_status_id
      WHERE
        a.ndemPots LIKE "%'.$id.'%" OR
        a.ndemSpeedy LIKE "%'.$id.'%" OR
        a.orderId LIKE "%'.$id.'%"
      ORDER BY
        b.updated_at DESC
    ');


    if (mysqli_num_rows($query)>0){
      $result = mysqli_fetch_array($query,MYSQLI_ASSOC);
      $content .= "=====================\n";
			$content .= "SC.".$result['orderId']."\n";
      $content .= $result['orderStatus']."\n";
      $content .= $result['jenis_layanan']."\n";
      $content .= "STO : ".$result['sto']."\n";
      $content .= $result['alproname']."\n";
      $content .= "NCLI : ".$result['orderNcli']."\n";
      $content .= "<b>".$result['ndemPots']."</b>\n";
      $content .= "<b>".$result['ndemSpeedy']."</b>\n";
      $content .= "<b>".$result['orderName']."</b>\n";
      $content .= $result['kcontact']."\n";
      $content .= $result['orderAddr']."\n";
	  $content .= "=====================\n";
      if ($result['uraian'] <> NULL) {
		$content .= "Dispatch at : ".$result['updated_at']."\n";
        $content .= "Helpdesk : ".$result['updated_by']."\n";
        $content .= "Keterangan Manja : \n<i>".$result['manja']."</i>\n";
        $content .= "Regu : <b>".$result['uraian']."</b>\n";
        $content .= "Status : <b>".$result['status_teknisi']."</b>\n";
		$content .= "Kordinat Pelanggan (T) : ".$result['kordinat_pelanggan']."\n";
		$content .= "Keterangan Teknisi : \n<i>".$result['catatan']."</i>\n";
      } else {
        $content .= "\nNOT ASSIGN\n";
      }
      $content .= "=====================\n";
      
     
    } else {
      $content .= "\n".$id." ORDER NOT FOUND\n";
    }
    return $content;
  }
  
  function checkwo_getfile($con,$filename, $id){
	$query = mysqli_query($con,'SELECT id FROM dispatch_teknisi WHERE Ndem = "'.$id.'" group by id');
	if (mysqli_num_rows($query)>0){
		$datax = mysqli_fetch_array($query,MYSQLI_ASSOC);
		$wo = $datax['id'];
		$path = __DIR__."/upload/";
		$file = $path."evidence/".$wo."/".$filename.".jpg";
    } else {
		$file = "FILE NOT FOUND";
	}
	return $file;
  }

 

?>
