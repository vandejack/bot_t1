<!DOCTYPE html>
<head>
    <style>
        body {
            background: white;
            font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
        }
        td,th {
            padding: 5px;
            line-height: 1.3em;
        }
        td.key {
            color: black;
        }
        td.value {
            color: gray;
        }
        th.mgrid {
            background-color: rgb(0,102,167);
            color: white;
            font-weight: normal;
        }
        td.mgrid {
            background-color: rgb(121,202,255);
            color: black;
        }
        th.grid {
            background-color: gray;
            color: white;
            font-weight: normal;
        }
        td.grid {
            background-color: silver;
            color: black;
        }
        img { display:none; }
        #logo {
            display: block;
            position: absolute;
            right: 10px;
            top: 20px;
        }
    </style>
</head>
<body>
    <img id="logo" src="../../../img/tomman.png" />
    <?= $html ?>
</body>