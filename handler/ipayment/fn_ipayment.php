<?php

define('IPAYMENT_URL', 'http://10.60.165.60/script/');
//define('IPAYMENT_URL', 'http://10.0.40.236/script/');

function ipayment_request($jastel) {
    $param = '&rname=Hadi%20Susilo&raddr=Banjarmasin&rphone=081253680725';
    
    $cookie = ipayment_getCookie();
    $context = stream_context_create([
        'http' => [
            'method' => 'GET',
            'header' => "Cookie: $cookie\r\n"
        ]
    ]);
    
    $url = 'intag_search.php?phone='.$jastel.$param;
    $result = @file_get_contents(IPAYMENT_URL.$url, false, $context);
    if ($result === FALSE) throw new Exception('iPayment: REQUEST FAILED');
    
    return $result;
}

function ipayment_getCookie() {
    $cookie = null;
    
    // TODO: temporary files in specific folder
    if (file_exists('cookie.tmp')) {
        clearstatcache();
        $age = time() - filemtime('cookie.tmp');
        if ($age < 1000) {
            $cookie = file_get_contents('cookie.tmp');
        }
    }
    
    if (!$cookie) {
        $cookie = ipayment_login();
        file_put_contents('cookie.tmp', $cookie);
    }
    
    return $cookie;
}

function ipayment_login() {
    $user = '610407';
    $pass = $user;
    
    $postData = http_build_query([
        'uid' => $user,
        'pwd' => $pass
    ]);
    
    $loginContext = stream_context_create([
        'http' => [
            'method' => 'POST',
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'content' => http_build_query([
                'uid' => $user,
                'pwd' => $pass
            ])
        ]
    ]);
    
    $result = @file_get_contents(IPAYMENT_URL.'intag_login.php', false, $loginContext);
    
    // GET login? SERIOUSLEE???!!!
    $result = @file_get_contents(IPAYMENT_URL."intag_login.php?uid={$user}&pwd={$pass}");
    
    $headers = $http_response_header;
    $cookie = false;
    foreach($headers as $h) {
        $tok = explode(' ', $h);
        if ($tok[0] == 'Set-Cookie:') $cookie = substr($h,12);
    }
    if (!$cookie) throw new Exception('iPayment: LOGIN FAILED');

    return $cookie;
}

function ipayment_output($name,$html) {
    $trim = 'display:none';
    $pos = strpos($html, $trim);
    if ($pos !== FALSE) $html = substr_replace($html, '', $pos, strlen($trim));
    $basedir = __DIR__;
    
    echo "[D] basedir:$basedir\n";
    
    ob_start();
    include $basedir.'/template.php';
    $output = ob_get_contents();
    ob_end_clean();
    
    $fname = $basedir."/out/$name.html";
    
    
    file_put_contents($fname, $output);
    passthru("phantomjs $basedir/rasterize.js file:///$fname $basedir/out/$name.png \"980px\"");
}