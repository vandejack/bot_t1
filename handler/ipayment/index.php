<?php

require_once('fn_ipayment.php');

if (count($token)>1) $jastel = trim($token[1]); else $jastel = "";

if (!$jastel) {
    $tgram->sendMessage([
        'chat_id' => $chatid,
        'text' => "silahkan input nomor jastel, misal:\n/ipayment 051112345\n/ipayment 161123154654"
    ]);
    return;
}

$tgram->sendMessage([
    'chat_id' => $chatid,
    'text' => "PROCESSING: I-Payment $jastel"
]);
try {
    $result = ipayment_request($jastel);
    ipayment_output($jastel,$result);

    $path = __DIR__."/out/$jastel";

    $tgram->sendPhoto([
        'chat_id' => $chatid,
        'photo' => $path.".png"
    ]);

    unlink($path.".png");
    unlink($path.".html");
}
catch (Exception $e) {
    $tgram->sendMessage([
        'chat_id' => $chatid,
        'text' => get_class($e)."\n".$e->getMessage()
    ]);
}
